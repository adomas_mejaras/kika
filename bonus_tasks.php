<!DOCTYPE html>
<html>
<head>
    <title>Password Generator</title>
    <script>
        function copyToClipboard() {
            var text = document.getElementById("generated_password");
            text.select();
            document.execCommand("copy");
            alert("Password copied to clipboard!");
        }
    </script>
</head>
<body>
<form action="" method="post">
    <label for="length">Password Length:</label>
    <select name="length" id="length">
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
    </select>
    <br>
    <br>
    <br>
    <label for="complexity">Password Complexity:</label>
    <select name="complexity">
        <option value="alpha_numeric">Alphanumeric</option>
        <option value="alpha_numeric_special">Alphanumeric with Special Characters</option>
    </select>
    <br>
    <br>
    <input type="submit" name="generate_password" value="Generate Password">
    <br>
    <br>
</form>
<?php
if (isset($_POST['generate_password'])) {
    $length = $_POST['length'];
    $complexity = $_POST['complexity'];

    if ($complexity === "alpha_numeric") {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    } else {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=';
    }
    $password = substr(str_shuffle($chars), 0, $length);
    echo "<input type='text' id='generated_password' value='$password' readonly>";
    echo "<button onclick='copyToClipboard()'>Copy to Clipboard</button>";
    echo "<br>";
    echo "<br>";
    echo "<form action='save-password.php' method='post'>";
    echo "<input type='hidden' name='password' value='$password'>";
    echo "<input type='submit' value='Save Password to File'>";
    echo "</form>";
}
?>
</body>
</html>
