<!DOCTYPE html>
<html>
<head>
  <title>Random Password Generator</title>
</head>
<body>
  <form method="POST">
    <input type="submit" name="generate_password" value="Generate Password">
  </form>

  <?php
  if (isset($_POST['generate_password'])) {
    // characters to choose from for the password
    $lowercase = 'abcdefghijklmnopqrstuvwxyz';
    $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $numbers = '0123456789';
    $combined = $lowercase.$uppercase.$numbers;

    // choose a random length for the password between 8 and 12 characters
    $password_length = rand(8, 12);

    // initialize the password as an empty string
    $password = '';

    // add one random lowercase letter to the password
    $password .= $lowercase[rand(0, strlen($lowercase) - 1)];

    // add one random uppercase letter to the password
    $password .= $uppercase[rand(0, strlen($uppercase) - 1)];

    // add one random number to the password
    $password .= $numbers[rand(0, strlen($numbers) - 1)];

    // add random characters to the password to reach the desired length
    for ($i = 0; $i < $password_length - 3; $i++) {
      $password .= $combined[rand(0, strlen($combined) - 1)];
    }

    // shuffle the password to make it more random
    $password = str_shuffle($password);

    // display the generated password on the page
    echo '<p>Generated password: ' . $password . '</p>';
  }
  ?>
</body>
</html>
